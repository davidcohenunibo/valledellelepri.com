<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">

<!-- Modal -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="h4 modal-title text-center" id="myModalLabel">Ci dispiace ma la quantità presente nel magazzino esaurita</p>
            </div>
        </div>
    </div>
</div>

<!-- Product -->
<div class="shopping-cart">
    <div class="container">
        <div class="block-heading">
            <p class="h2">Carrello</p>
        </div>
        <div class="content">
            <div class="row">
                <div class="col-md-12 col-lg-8">
                    <div class="items">
                        <!-- ADVISE THAT THERE ARE NO PRODUCT IN CART -->
                        
                        <div id="noProductAllert" class="alert alert-warning">
                            Non ci sono articoli nel carrello. <a href="index.php" class="alert-link">Clicca qui per tornare gli acquisti!</a>
                        </div>
                        <!-- SHOW PRODUCT -->
                        <?php foreach($cart["articleDetails"] as $article): ?> 
                        <div id="<?php echo "P_" . $article["id"]."_".$article["taglia"] ?>" class="product">
                            <div class="row">
                                <div class="col-md-3">
                                    <img class="img-fluid mx-auto d-block image product-image" src="<?php echo IMG_DIR.$article["id"].".jpg" ?>" alt="<?php echo IMG_DIR.$article["nome"] ?>">
                                </div>
                                <div class="col-md-8">
                                    <div class="info">
                                        <div class="row">
                                            <div class="col-md-5 product-name">
                                                <div class="product-name">
                                                    <?php echo $article["nome"] ?>
                                                    <div class="product-info">
                                                        <div>Colore: <span class="value">Bianco</span></div>
                                                        <div>Taglia: <span class="value"><?php echo $article["taglia"] ?></span></div>
                                                        <div>Genere: <span class="value">Uomo</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md quantity">
                                                <div class="input-group product-group">
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-btn">
                                                            <input type="button" value="-" class="btn btn-danger" 
                                                                onclick="decreaseQuantity('<?php echo $article["id"]."','".$article["taglia"] ?>')">
                                                        </div>
                                                    </div>
                                                    <div id="<?php echo "Q_" . $article["id"]."_".$article["taglia"] ?>" class="form-control product-quantity">
                                                        <?php echo $article["quantita"]?>
                                                    </div>
                                                    <div class="input-group-prepend">
                                                        <div class="input-group-btn">
                                                            <input type="button" value="+" class="btn btn-success" 
                                                                onclick="increaseQuantity('<?php echo $article["id"]."','".$article["taglia"] ?>')" >
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div id="<?php echo "C_" . $article["id"]."_".$article["taglia"] ?>" class="col-md-3 price product-price">
                                                <?php echo ($article["prezzo"] * $article["quantita"]) ?>€
                                            </div>
                                            <div class="col-md">
                                                <button class="btn trash-btn" onclick="deleteProduct('<?php echo $article["id"]."','".$article["taglia"] ?>')">
                                                    <span class="fa fa-trash"></span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php endforeach ?>
                        <!-- END SHOW PRODUCT -->
                    </div>
                    <button id="trashAllBtn" class="btn trash-btn" onclick="deleteAllProduct()">
                        Svuota carrello <span class="fa fa-trash"></span>
                    </button>
                </div>
                <div class="col-md-12 col-lg-4">
                    <div class="summary">
                        <p class="h4 font-weight-bold text-center">Resoconto</p>
                        <div class="summary-item"><span class="text">Totale: </span><span id="totalPrice" class="price"><?php echo $cart["totalPrice"] ?>€</span></div>
                        <button type="button" class="btn btn-primary btn-lg btn-block" onclick="location.href = 'checkout.php';">Checkout</button>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>

<!-- shop icon -->
<script src="https://use.fontawesome.com/c560c025cf.js"></script>
<!-- script that need php rendering -->
<script>
    $(function() {
        const phpCheck = <?php if(empty($cart["articleDetails"])) {echo "true";} else {echo "false";} ?>;
        if(phpCheck) {
            $("#noProductAllert").show();
            $("#trashAllBtn").hide();
        }
        else {
            $("#noProductAllert").hide();
            $("#trashAllBtn").show();
        }
    });
</script>
