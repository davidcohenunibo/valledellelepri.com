<div class="col-12">
<div class="modal fade right" id="modalAbandonedCart" tabindex="-1" role="dialog" aria-labelledby="modalAbandonedCart"
  aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-side modal-bottom-right modal-notify modal-info" role="document">
    <!--Content-->
    <div class="modal-content">
      <!--Header-->
      <div class="modal-header">
        <h3 class="heading"><?php echo  "[".$_GET["quantita"]."] ".$templateParams["idMarca"]["nome"]." ".$prodotto["nome"]." " ?>nel carrello!</h3>
      </div>

      <!--Body-->
      <div class="modal-body">

        <div class="row">
          <div class="col-3">
            <p></p>
            <p class="text-center"><span class="fa fa-shopping-cart fa-4x" aria-hidden="true"></span></p>
          </div>

          <div class="col-9">
            <p>Hai bisogno di più tempo per decidere?</p>
            <p>Nessun problema, le tue <?php echo $templateParams["idMarca"]["nome"] ?> ti aspetteranno nel carrello.</p>
          </div>
        </div>
      </div>

      <!--Footer-->
      <div class="modal-footer justify-content-center">
      <a href="index.php" id="btnReturnHome" class="btn-addToCart btn btn-info btn-rounder" >Continua a Comprare</a>
      <a href="cart.php" id="btnGoToCart" class="btn-addToCart btn btn-info btn-rounder" >Vai al Carrello</a>
      </div>
    </div>
    <!--/.Content-->
  </div>
</div>
</div>
<!-- Modal -->