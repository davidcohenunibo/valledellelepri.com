<?php

define("SHOP_NAME", "Valle Delle Lepri");
define("CART_COOKIE", "CART_COOKIE");
define("CSS_FILE", "./css/");
define("JS_FILE", "./js/");
define("BOOTSTRAP_CSS_LINK", "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css");
define("BOOTSTRAP_JS_LINK", "https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js");
define("JQUERY_LINK", "https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js");
define("IMG_DIR", "./upload/");
define("PROMOTION_DIR", "./upload/promotion/");

define("DB_SERVER_NAME", "127.0.0.1");
define("DB_USERNAME", "root");
define("DB_PASSWORD", "");
define("DB_NAME", "ValleDelleLepri");
define("MAX_PRODUCT_FOR_PAGE", 6);

//TODO: correct fields
define("CREATORS", array(
    1 => array(
        "nome" => "Luigi",
        "cognome" => "Olivieri",
        "email" => "luigi.olivieri@studio.unibo.it",
        "matricola" => "880401"
    ),
    2 => array(
        "nome" => "David",
        "cognome" => "Cohen",
        "email" => "david.cohen@studio.unibo.it",
        "matricola" => "TODO"
    ),
    3 => array(
        "nome" => "Lorenzo",
        "cognome" => "Morelli",
        "email" => "lorenzo.morelli9@studio.unibo.it",
        "matricola" => "882168"
    )
));
